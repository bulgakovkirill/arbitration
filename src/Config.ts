export default {
    Redis: {
        host: process.env.REDIS_HOST || '127.0.0.1',
        port: 6379
    },

    Funds: {
        usdAmount: 4850
    },

    WatchingInterval: 10000,

    Telegram: {
        apiUrl: "https://api.telegram.org/bot334627005:AAEyT23_JmKsbGa9LugA_484iFxf37ciIws/",
        chat_id: "47435058"
    }
};
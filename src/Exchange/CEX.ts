import {IExchange} from "./IExchange";
import {IOrderBook, mapToOrder} from "./IOrderBook";
import {Currency} from "./Currency";
import {Request} from "../Request/Request";

export class CEX implements IExchange {
    private static instance: CEX;

    static getInstance(): IExchange {
        return this.instance || (this.instance = new this());
    }

    async getOrderBook(baseCurrency: Currency, quoteCurrency: Currency): Promise<IOrderBook> {
        const pair = `${baseCurrency}/${quoteCurrency}`;
        const [err, orderBook] = await Request.makeRequest("GET", `https://cex.io/api/order_book/${pair}`, 3000);

        if (!orderBook || !orderBook.asks || !orderBook.bids) {
            console.error(`getOrderBook, args: ${Array.prototype.slice.call(arguments)}, time: ${new Date()}`, err || "Not existed pair");

            return {asks: [], bids: []};
        }

        orderBook.asks = mapToOrder(orderBook.asks);
        orderBook.bids = mapToOrder(orderBook.bids);

        return orderBook;
    };

    buyCurrency(currency: Currency, price: number, amount: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    sellCurrency(currency: Currency, price: number, amount: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}
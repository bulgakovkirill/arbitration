import {IExchange} from "./IExchange";
import {IOrder, IOrderBook, mapToOrder} from "./IOrderBook";
import * as request from "request-promise";
import {Currency} from "./Currency";
import {Request} from "../Request/Request";

export class DSX implements IExchange {
    private static instance: DSX;

    static getInstance(): IExchange {
        return this.instance || (this.instance = new this());
    }

    async getOrderBook(baseCurrency: Currency, quoteCurrency: Currency): Promise<IOrderBook> {
        const pair = `${baseCurrency.toLowerCase()}${quoteCurrency.toLowerCase()}`;

        const [err, orderBook] = await Request.makeRequest("GET", `https://dsx.uk/mapi/depth/${pair}`, 3000);

        if (!orderBook || !orderBook[pair]) {
            console.error(`getOrderBook, args: ${Array.prototype.slice.call(arguments)}, time: ${new Date()}`, err || "Not existed pair");

            return {asks: [], bids: []};
        }

        orderBook[pair].asks = mapToOrder(orderBook[pair].asks);
        orderBook[pair].bids = mapToOrder(orderBook[pair].bids);

        return orderBook[pair];
    };

    buyCurrency(currency: Currency, price: number, amount: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    sellCurrency(currency: Currency, price: number, amount: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}
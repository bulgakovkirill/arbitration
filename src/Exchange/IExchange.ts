import {IOrderBook, OrderType} from "./IOrderBook";
import {Currency} from "./Currency";

export interface IExchange {
    getOrderBook(baseCurrency: Currency, quoteCurrency : Currency): Promise<IOrderBook>,

    buyCurrency(currency: Currency, price: number, amount: number): Promise<boolean>,

    sellCurrency(currency: Currency, price: number, amount: number): Promise<boolean>
}
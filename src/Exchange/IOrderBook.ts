export interface IOrder {
    price: number,
    amount: number,
}

export interface IOrderBook {
    asks: IOrder[],
    bids: IOrder[],
}

export type OrderType = "ask" | "bid";

export const mapToOrder = (sourceArray: Array<any>): Array<IOrder> =>
    sourceArray.map(order =>
        Object({
            price: order[0],
            amount: order[1]
        }));
import {DSX} from "./Exchange/DSX";
import {CEX} from "./Exchange/CEX";
import Config from "./Config";
import {calculateVolumeByOrders} from "./Utils/Utils";
import {Telegram} from "./Telegram/Telegram";

const watcherEtherFunc = async () => {
    const [dsxOrderBook, cexOrderBook] = await Promise.all([
        DSX.getInstance().getOrderBook("ETH", "USD"),
        CEX.getInstance().getOrderBook("ETH", "USD")
    ]);

    if (!dsxOrderBook.asks || !dsxOrderBook.asks.length) {
        console.error("DSX order book is empty");
        return;
    }

    if (!cexOrderBook.asks || !cexOrderBook.asks.length) {
        console.error("CEX order book is empty");
        return;
    }

    const ethAmountOnDSX = calculateVolumeByOrders(Config.Funds.usdAmount, dsxOrderBook.asks, "ask");
    const usdAmountOnCEX = calculateVolumeByOrders(ethAmountOnDSX, cexOrderBook.bids, "bid");

    if (usdAmountOnCEX - Config.Funds.usdAmount < 120) {
        console.log(`ethArbitrage profit = ${usdAmountOnCEX - Config.Funds.usdAmount}`);
        return;
    }

    Telegram.sendMessage(`ethArbitrage profit = ${usdAmountOnCEX - Config.Funds.usdAmount}`);
};

const watcherBitcoinFunc = async () => {
    const [dsxOrderBook, cexOrderBook] = await Promise.all([
        DSX.getInstance().getOrderBook("BTC", "USD"),
        CEX.getInstance().getOrderBook("BTC", "USD")
    ]);

    if (!dsxOrderBook.asks || !dsxOrderBook.asks.length) {
        console.error("DSX order book is empty");
        return;
    }

    if (!cexOrderBook.asks || !cexOrderBook.bids.length) {
        console.error("CEX order book is empty");
        return;
    }

    const btcAmountOnDSX = calculateVolumeByOrders(Config.Funds.usdAmount, dsxOrderBook.asks, "ask");
    const usdAmountOnCEX = calculateVolumeByOrders(btcAmountOnDSX, cexOrderBook.bids, "bid");

    if (usdAmountOnCEX - Config.Funds.usdAmount < 120) {
        console.log(`btcArbitrage profit = ${usdAmountOnCEX - Config.Funds.usdAmount}`);
        return;
    }

    Telegram.sendMessage(`btcArbitrage profit = ${usdAmountOnCEX - Config.Funds.usdAmount}`);
};

setInterval(watcherEtherFunc, Config.WatchingInterval);
setInterval(watcherBitcoinFunc, Config.WatchingInterval);

/*

{ asks:
   [ { price: 0.06487, amount: 14.5546 },
     { price: 0.06492, amount: 4.4079 },
     { price: 0.06495, amount: 20.4554 },
     { price: 0.06496, amount: 42.2308 },
     { price: 0.06498, amount: 1.7303 },
     { price: 0.06499, amount: 12 },
     { price: 0.06501, amount: 0.65 },
     { price: 0.06504, amount: 83.4739 },
     { price: 0.06506, amount: 15.7737 },
     { price: 0.06507, amount: 7.752 },
     { price: 0.06508, amount: 0.5873 },
     { price: 0.06511, amount: 7.925 },
     { price: 0.079, amount: 6.3 },
     { price: 0.111, amount: 1 },
     { price: 0.131, amount: 0.5 },
     { price: 0.161, amount: 0.5 },
     { price: 0.4997, amount: 2 } ],
  bids:
   [ { price: 0.06418, amount: 4 },
     { price: 0.06417, amount: 33.1477 },
     { price: 0.06416, amount: 24.7895 },
     { price: 0.06415, amount: 33.3257 },
     { price: 0.06413, amount: 7.925 },
     { price: 0.06412, amount: 9.2906 },
     { price: 0.0641, amount: 20.6587 },
     { price: 0.06406, amount: 41.4963 },
     { price: 0.06403, amount: 13.1029 },
     { price: 0.06402, amount: 12.499 },
     { price: 0.06401, amount: 1.0619 },
     { price: 0.064, amount: 3.1008 },
     { price: 0.06397, amount: 3.1006 },
     { price: 0.06395, amount: 7.7564 },
     { price: 0.06394, amount: 7.752 },
     { price: 0.06389, amount: 7.7524 },
     { price: 0.011, amount: 5 } ],
  market_buy_price: 0.4997,
  market_sell_price: 0.011 }

 */
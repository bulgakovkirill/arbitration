// export class Monitor {
//     private static instance: Monitor;
//     public interval : number;
//     private timer : NodeJS.Timer;
//
//     static getInstance(): Monitor {
//         return this.instance || (this.instance = new this());
//     }
//
//     start(interval : number = 1000, strategy : IStrategy) {
//         this.interval = interval;
//
//         this.timer = setInterval( () => {
//             strategy.nextTick();
//         }, this.interval);
//     }
//
//     getConfigure() : MonitorConfigure {
//         return {
//             interval : this.interval
//         }
//     }
// }
//
// export interface MonitorConfigure {
//     interval : number
// }
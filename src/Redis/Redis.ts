import * as redis from "redis";
import Config from "../Config";

export class Redis {
    private static instance : Redis;
    private client: redis.RedisClient;

    static getInstance () {
        return this.instance ? this.instance : this.instance = new Redis();
    }

    private constructor() {
        this.client = redis.createClient(Config.Redis);
    }

    public get = async (key: string) : Promise<string> =>
        new Promise<string>((resolve, reject) => this.client.get(key, (err, response) => err ? reject(err) : resolve(response)));

    public set = async (key: string, value: string) =>
        new Promise((resolve, reject) => this.client.set(key, value, (err, response) => err ? reject(err) : resolve()));
}
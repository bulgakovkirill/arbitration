import * as request from "request-promise";

export class Request {
    static async makeRequest(method: Method, url : string, timeout : number, form? : Object) : Promise<Errorable> {
        try {
            const result = JSON.parse(await request({method, url, timeout, form}));

            return [null, result];
        } catch (e) {
            return [e, null];
        }
    }
}

export type Errorable = [Error | null, any];

export type Method = "GET" | "POST";
import {Request} from "../Request/Request";
import Config from "../Config";

export class Telegram {
    static async sendMessage(text: string) {
        const [err, _] = await Request.makeRequest("POST",
            `${Config.Telegram.apiUrl}sendMessage`,
            3000,
            {
                text: text,
                chat_id: Config.Telegram.chat_id
            }
        );

        if (err) {
            console.error(`sendMessage, args: ${Array.prototype.slice.call(arguments)}`, err);
            return;
        }
    }
}
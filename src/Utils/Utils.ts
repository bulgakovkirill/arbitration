import {IOrder, OrderType} from "../Exchange/IOrderBook";

/***
 * Calculate how much quote currency you can buy when provided `ask` order type
 * Calculate how much base currency will you get when provided `bids` order type
 *
 * @param {number} currencyAmount - amount of currency which you have
 * @param {IOrder[]} orders - array of orders from order book
 * @param {OrderType} type - which orders was passed to this function: asks or bids
 * @returns {number}
 */
export const calculateVolumeByOrders = (currencyAmount: number, orders: IOrder[], type: OrderType): number => {
    let result = 0;

    if (type === "ask") {
        orders.forEach(order => {
            if (currencyAmount === 0) return;

            if (currencyAmount >= order.amount * order.price) {
                currencyAmount -= order.amount * order.price;
                result += order.amount;
            } else {
                result += currencyAmount / order.price;
                currencyAmount = 0;
            }
        });

        return result;
    }

    orders.forEach(order => {
        if (currencyAmount === 0) return;

        if (currencyAmount >= order.amount) {
            currencyAmount -= order.amount;
            result += order.price * order.amount;
        } else {
            result += currencyAmount * order.price;
            currencyAmount = 0;
        }
    });

    return result;
};
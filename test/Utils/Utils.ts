import {assert} from 'chai';
import {calculateVolumeByOrders} from "../../src/Utils/Utils";
import 'mocha';

describe('Utils tests', () => {
    it("calculateVolumeByOrders with `ask`#1", () => {
        const result = calculateVolumeByOrders(1000,
            [
                {
                    price: 500,
                    amount: 1
                },
                {
                    price: 500,
                    amount: 1
                }
            ], "ask");

        assert.equal(result, 2);
    });

    it("calculateVolumeByOrders with `ask`#2", () => {
        const result = calculateVolumeByOrders(100,
            [
                {
                    price: 30,
                    amount: 1
                },
                {
                    price: 35,
                    amount: 2
                }
            ], "ask");

        assert.equal(result, 3);
    });

    it("calculateVolumeByOrders with `bid`#1", () => {
        const result = calculateVolumeByOrders(5,
            [
                {
                    price: 20,
                    amount: 3
                },
                {
                    price: 10,
                    amount: 1
                }
            ], "bid");

        assert.equal(result, 70);
    });

    it("calculateVolumeByOrders with `bid`#2", () => {
        const result = calculateVolumeByOrders(1,
            [
                {
                    price: 20,
                    amount: 3
                },
                {
                    price: 10,
                    amount: 1
                }
            ], "bid");

        assert.equal(result, 20);
    });
});